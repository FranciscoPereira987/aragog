
|Requerimiento|Estado|Comentarios|Tareas realcionadas|
|:--:|:--:|:--:|:--:|
|R01-Implementacion robots.txt protocol|NO inicializado|[Info](https://www.cloudflare.com/learning/bots/what-is-robots-txt)[More Info](https://moz.com/learn/seo/robotstxt)|||
|R02-Implementacion API de crawling|NO inicializado|||
|R03-Implementacion Base de datos|NO inicializado|||
|S01-Implementacion crawling por niveles|NO inicializado||Subtarea de R02|
|S02-path methods|NO inicializado||Subtarea de R01|
|S03-sitemaps methods|NO inicializado||Subtarea de R01|
|S04-Parser robots.txt|Implementando|Evaluar tema de multiples user-agent en un mismo Robots.txt|Subtarea de R01|

