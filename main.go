package main

import (
	"fmt"
	"io"
	"net/http"
)

func ReadAll(from io.Reader) ([]byte, error) {
	result := make([]byte, 1)
	total_readed := 0

	readed, err := from.Read(result)

	for readed > 0 && err == nil {
		total_readed += readed
		new_results := make([]byte, len(result))
		readed, err = from.Read(new_results)
		result = append(result, new_results...)
	}

	total_readed += readed

	if err != io.EOF {
		return nil, err
	}

	return result[:total_readed], nil
}

func main() {
	resp, _ := http.Get("https://www.youtube.com/robots.txt")

	response, _ := ReadAll(resp.Body)

	fmt.Println(string(response))
}
