package utils

import (
	"reflect"
	"strings"
	"testing"
)

type parseTestCase struct {
	name   string
	lines  string
	result []string
}

type fieldParseTestCase struct {
	name   string
	lines  string
	fields []string
	result map[string][]string
}

func TestLinesParsing(t *testing.T) {
	testCases := []parseTestCase{
		{"Parse one line", "line of text", []string{"line of text"}},
		{"Parse two lines", "first line\nsecond line", []string{"first line", "second line"}},
		{"Parse some fields", "User-agent: *\nDissalow: \\uri1\nAllow: \\parser",
			[]string{"User-agent: *", "Dissalow: \\uri1", "Allow: \\parser"}},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			parser := newParserUtil(strings.NewReader(test.lines))
			result := make([]string, 0)

			for !parser.ended() {
				result = append(result, parser.parseLine())
			}

			if len(result) != len(test.result) {
				t.Logf("Length result %d not equal to %d", len(result), len(test.result))
				t.FailNow()
			}

			for index, line := range result {
				if line != test.result[index] {
					t.Logf("%s != %s", line, test.result[index])
					t.FailNow()
				}
			}
		})
	}
}

func TestFieldsParsing(t *testing.T) {
	testCases := []fieldParseTestCase{
		{"parse one field", "fieldName: value",
			[]string{"fieldName"},
			map[string][]string{"fieldName": {"value"}}},

		{"do not parse undefined fields",
			"undefined: value1\ndefined: value2\nnotAllowed: value4",
			[]string{"defined", "notAllowed"},
			map[string][]string{
				"defined":    {"value2"},
				"notAllowed": {"value4"},
			}},

		{"parse same field multiple times",
			"allow: value1\nallow: value2\nallow:value3\nallow: value4",
			[]string{"allow"},
			map[string][]string{
				"allow": {"value1", "value2", "value3", "value4"},
			}},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			parser := NewfieldParser(strings.NewReader(test.lines))
			parser.AddFields(test.fields...)

			result := parser.ParseFields()

			if !reflect.DeepEqual(result, test.result) {
				t.Logf("Testcase: %s result and parsed fields were not equal\n%s != %s", test.name, result, test.result)
				t.FailNow()
			}

			result = parser.ParseFields()

			if !reflect.DeepEqual(result, test.result) {
				t.Logf("Testcase: %s fields after parsing not being correctly returned", test.name)
				t.FailNow()
			}

		})
	}
}
