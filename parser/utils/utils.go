package utils

import (
	"io"
	"strings"
)

var (
	newLine = '\n'
	colon   = ':'
)

/*
util to parse robots.txt with a buffer
*/
type parserUtil struct {
	reader io.Reader
	buffer string
	eof    bool
}

func newParserUtil(from io.Reader) *parserUtil {
	return &parserUtil{
		reader: from,
		buffer: "",
		eof:    false,
	}
}

/*
Advances the the read by the amount of bytes specified
*/
func (this *parserUtil) advanceRead(by int) (bool, string) {
	prebuff := make([]byte, by)

	total, err := this.reader.Read(prebuff)

	if err != nil && err != io.EOF {
		return true, ""
	}

	return total < by, string(prebuff[:total])

}

/*
Parses a complete line of text or till the end
*/
func (this *parserUtil) parseLine() string {
	eof, line := this.advanceTillNewline()
	return this.updateState(line, eof)
}

func (this *parserUtil) advanceTillNewline() (eof bool, line string) {
	total := 1
	eof = this.eof
	line = this.buffer
	var line_cont string

	for !eof && !strings.ContainsRune(line, newLine) {
		eof, line_cont = this.advanceRead(total)
		line += line_cont
		total *= 2
	}

	return
}

func (this *parserUtil) updateState(line string, eof bool) (result string) {
	this.eof = eof
	if !eof {
		newlineIndex := strings.IndexRune(line, newLine)
		this.buffer = line[newlineIndex+1:]
		line = line[:newlineIndex]
	}
	result = line
	return
}

/*
If EOF has been reach or an error during the reading has occured, returns true.
Otherwise returns false
*/
func (this *parserUtil) ended() bool {
	return this.eof
}

/*
Field parser util to parse fields in a stream
*/
type FieldParser struct {
	fields map[string]bool

	lineParser *parserUtil

	parsed map[string][]string
}

func NewfieldParser(from io.Reader) *FieldParser {
	return &FieldParser{
		fields:     make(map[string]bool),
		lineParser: newParserUtil(from),
		parsed:     make(map[string][]string),
	}
}

func (this *FieldParser) AddFields(fields ...string) {
	for _, field := range fields {
		this.fields[field] = true
	}
}

func (this *FieldParser) getField() (field string, value string) {
	line := this.lineParser.parseLine()

	if strings.ContainsRune(line, colon) {
		colonIndex := strings.IndexRune(line, colon)
		field = line[:colonIndex]
		value = strings.TrimSpace(line[colonIndex+1:])
	}

	return
}

func (this *FieldParser) ParseFields() map[string][]string {

	for !this.lineParser.ended() {
		field, value := this.getField()
		if ok, _ := this.fields[field]; ok {
			this.parsed[field] = append(this.parsed[field], value)
		}
	}

	return this.parsed
}
