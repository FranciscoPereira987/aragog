package parser

import (
	"aragog/parser/utils"
	"io"
)

var (
	userAgent  = "User-Agent"
	sytemap    = "Sytemap"
	dissallow  = "Dissallow"
	allow      = "Allow"
	crawlDelay = "Crawl-delay"
)

/*
	Representation of the diferent fields that can appear on a robots.txt file
	based on a single useragent
*/
type RobotFields struct {
	UserAgent string

	Sytemap []string

	Dissallow []string

	Allow []string

	CrawlDelay string
}

func (this *RobotFields) buildFromFields(fields map[string][]string) {
	this.Sytemap = fields[sytemap]
	this.Dissallow = fields[dissallow]
	this.Allow = fields[allow]

	if value, ok := fields[userAgent]; ok {
		this.UserAgent = value[0]
	}

	if value, ok := fields[crawlDelay]; ok {
		this.CrawlDelay = value[0]
	}
}

func ParseRobots(from io.Reader) (*RobotFields, error) {
	parser := utils.NewfieldParser(from)

	parser.AddFields(userAgent, sytemap, dissallow, allow, crawlDelay)

	fields := parser.ParseFields()

	robots := new(RobotFields)

	robots.buildFromFields(fields)

	return robots, nil
}
