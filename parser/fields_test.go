package parser

import (
	"reflect"
	"strings"
	"testing"
)

func TestDisallowField(t *testing.T) {
	r := strings.NewReader("Dissallow: /somurl")

	robotsFile, err := ParseRobots(r)

	if err != nil {
		t.Logf("ParseRobots returned the following error: %s", err.Error())
		t.FailNow()
	}

	if robotsFile.Dissallow == nil || len(robotsFile.Dissallow) != 1 {
		t.Logf("Incorrectly parsed file: %s", robotsFile.Dissallow)
		t.FailNow()
	}

	if robotsFile.Dissallow[0] != "/somurl" {
		t.Logf("Parsed resource does not match test resource: %s", robotsFile.Dissallow[0])
		t.FailNow()
	}
}

func TestYoutubeRobots(t *testing.T) {
	r := strings.NewReader("# robots.txt file for YouTube\n# Created in the distant future (the year 2000) after\n# the robotic uprising of the mid 90's which wiped out all humans.\nUser-agent: Mediapartners-Google*\nDisallow:\nUser-agent: *\nDisallow: /comment\nDisallow: /feeds/videos.xml\nDisallow: /get_video\nDisallow: /get_video_info\nDisallow: /get_midroll_info\nDisallow: /live_chat\nDisallow: /login\nDisallow: /qr\nDisallow: /results\nDisallow: /signup\nDisallow: /t/terms\nDisallow: /timedtext_video\nDisallow: /verify_age\nDisallow: /watch_ajax\nDisallow: /watch_fragments_ajax\nDisallow: /watch_popup\nDisallow: /watch_queue_ajax\n\nSitemap: https://www.youtube.com/sitemaps/sitemap.xml\nSitemap: https://www.youtube.com/product/sitemap.xml")
	expected_dissalow := []string{"", "/comment", "/feeds/videos.xml", "/get_video", "/get_video_info", "/get_midroll_info", "/live_chat", "/login", "/qr", "/results", "/signup", "/t/terms", "/timedtext_video", "/verify_age", "/watch_ajax", "/watch_fragments_ajax", "/watch_popup", "/watch_queue_ajax"}
	expected_sitemaps := []string{"https://www.youtube.com/sitemaps/sitemap.xml", "https://www.youtube.com/product/sitemap.xml"}

	robotsFile, err := ParseRobots(r)

	if err != nil {
		t.Logf("ParseRobots returned the following error: %s", err.Error())
		t.FailNow()
	}

	if reflect.DeepEqual(expected_dissalow, robotsFile.Dissallow) {
		t.Logf("Incorrectly parsed file: %s", robotsFile.Dissallow)
		t.FailNow()
	}


	if reflect.DeepEqual(expected_sitemaps, robotsFile.Sytemap) {
		t.Logf("Incorrectly parsed file: %s", robotsFile.Sytemap)
		t.FailNow()
	}

}
